<?php

/**
 * We add a provision save to our task, for the site that we're about to create.
 */
function drush_gardens_import_pre_hosting_task() {
  $task =& drush_get_context('HOSTING_TASK');
  $output = array();
  $mode = drush_get_option('debug', FALSE) ? 'GET' : 'POST';

  // Make sure argument order is correct
  ksort($task->args);

  // On install/verify, save the named context
  if ($task->task_type === 'gardens_import') {
    // Build up a fake array of args for the site.
    $site_args = array(
      'context_type' => 'site',
      'master_url' => url('', array('absolute' => TRUE)),
      'root' => NULL,
      'uri' => $task->task_args['site_url'],
      'db_server' => hosting_context_name($task->task_args['db_server']),
      'platform' => hosting_context_name($task->ref->nid), 
    );
    $output = drush_backend_invoke_args('provision-save', array('@' . $task->task_args['site_url']), $site_args, $mode);
    
    $task->args[1] = '@' . $task->task_args['site_url'];
    $task->args[2] = $task->task_args['export_file'];
  }
  
}

