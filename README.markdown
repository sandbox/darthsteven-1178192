Hosting Drupal Gardens import
=============================

Installation
------------

This project comprises the two parts, one for Aegir's front-end: hostmaster, the other for the back-end: provision. To install you will need to move the contents of the `provision` directory to within your provision folder on the Aegir server, this is normally located at `/var/aegir/.drush/provision`. You MUST not leave the contents of the folder in the module directory.

Usage
-----

To import a site from Drupal Gardens, follow the steps below:

1. Export your site from Drupal Gardens and save the export file locally, do not unpack it!
2. Add a new platform to Aegir, this will create a verify task that will fail, this is expected.
3. You should now be able to add a new 'Drupal Gardens import' task, choosing the site URL you'd like to use, the database server and the location of the export file.
4. The task will run, if this succeeds a platform verify task will be run and this will automatically add a task to import the site into the Aegir front-end.

Tips
----

* You can install and enable the [Upload element](http://drupal.org/project/upload_element) module, this will allow you to upload the export file directly into Aegir instead of having to specify a path to the file.
* If you are importing multiple Drupal Gardens sites and want them to all end up using the same Aegir platform, first import them using the method above into different platforms, and then migrate your sites into one of the other Drupal Gardens platforms, and delete the old platforms.


About the Developers
--------------------

This project is currently maintained by developers at [ComputerMinds](http://www.computerminds.co.uk). We at ComputerMinds pride ourselves on offering quality [Drupal training](http://www.computerminds.co.uk/drupal-training), [Drupal development](http://www.computerminds.co.uk/drupal-development) and [Drupal consulting](http://www.computerminds.co.uk/drupal-consulting). Go Drupal!