<?php

/**
 * Implementation of hook_drush_command().
 */
function gardens_drush_command() {
  $items['provision-gardens_import'] = array(
    'description' => dt('Import a site from Drupal Gardens'),
    'arguments' => array(
      '@context_name' => 'Site to use as template for import',
      'export_file' => 'Path to export tarball from Drupal Gardens',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  
  return $items;
}

/**
 * Validator for the Drush command to import a Drupal Gardens export into Aegir.
 *
 * Validate that:
 *  - The platform doesn't already exist.
 *  - The archive file is present and readable.
 */
function drush_provision_gardens_import_validate($site = '', $file = '') {
  
  if (file_exists(d()->root)) {
    drush_set_error('DRUSH_DRUPAL_GARDENS_PLATFORM_EXISTS', dt('Platform path already exists'));
  }
  
  provision_file()
    ->exists($file)
    ->fail('Could not find the export file: @path.', 'DRUSH_DRUPAL_GARDENS_EXPORT_NOT_FOUND');
    
  provision_file()
    ->readable($file)
    ->fail('Could not read the export file: @path.', 'DRUSH_DRUPAL_GARDENS_EXPORT_NOT_READABLE');
}

/**
 * Drush command to import a Drupal Gardens export into Aegir.
 */
function drush_provision_gardens_import($site, $file) {
  
  $site_context = d($site);
  
 // Create a temporary directory to work in.
  $tmp_dir = sys_get_temp_dir();
  if (strrpos($tmp_dir, '/') == strlen($tmp_dir) - 1) {
    $tmp_dir .= 'drupal_gardens_' . time();
  }
  else {
    $tmp_dir .= '/drupal_gardens_' . time();
  }
  drupal_gardens_mkdir($tmp_dir);

  // Unpack the archive.
  provision_file()
    ->extract($file, $tmp_dir . '/extracted')
    ->succeed('Export tarball: @path extracted successfully.');

  // Fix the permissions.
  provision_file()
    ->chgrp($tmp_dir . '/extracted', 'www-data', TRUE)
    ->succeed('Owner for extracted platform set.')
    ->fail('Could not set owner for extracted platform.');

  provision_file()
    ->chmod($tmp_dir . '/extracted', 0770, TRUE)
    ->succeed('Permissions for extracted platform set.')
    ->fail('Failed to set permissions for the extracted platform.');

  // Move the site directory in the correct place.
  provision_file()
    ->mkdir($tmp_dir . '/extracted/docroot/sites/' . $site_context->uri)
    ->succeed('Created site specific directory: @path')
    ->fail('Creation of site specific directory failed: @path');
  
  // Copy the default.settings.php into the site specific dir.
  provision_file()
    ->copy($tmp_dir . '/extracted/docroot/sites/default/default.settings.php', $tmp_dir . '/extracted/docroot/sites/' . $site_context->uri . '/default.settings.php')
    ->succeed('default.settings.php copied from platform.')
    ->fail('default.settings.php could not be copied form the platform');
  
  // Swap the default and site specific directories, Drupal Gardens puts your
  // site in /default where we want it to be in the correct domain specific
  // folder.
  provision_file()
    ->switch_paths(
      $tmp_dir . '/extracted/docroot/sites/' . $site_context->uri,
      $tmp_dir . '/extracted/docroot/sites/default'
    )
    ->succeed('Swapped site instance directory into the correct place.')
    ->fail('Could not swap the site instance directory into the correct place.');
    
  // Find the DB dump file.
  $dump_file = '';
  foreach (drush_scan_directory($tmp_dir . '/extracted/docroot', '#.*\.sql$#', array('.', '..', 'CVS'), 0, FALSE) as $dump_file => $v);
  
  if (empty($dump_file) || !file_exists($dump_file)) {
    return drush_set_error('DRUSH_DRUPAL_GARDENS_DB_DUMP_NOT_FOUND', dt('Could not find a database dump in the extracted platform'));
  }
  
  // Create the site database, and import it.
  $site_context->service('db')->connect();
  $creds = $site_context->service('db')->generate_site_credentials();
  $creds['db_type'] = drush_set_option('db_type', 'mysql', 'site');
  $site_context->service('db')->create_site_database($creds);
  $site_context->service('db')->import_dump($dump_file, $creds);
  
  // Delete the DB dump file
  provision_file()
    ->unlink($dump_file)
    ->succeed('Removed the imported DB dump: @path')
    ->fail('Could not remove the imported DB dump: @path', 'DRUSH_DRUPAL_GARDENS_DB_DUMP_STILL_PRESENT');
  
  // Create a new settings.php for the site with our new DB details.
  provision_prepare_environment();
  $config = new provisionConfig_drupal_settings($site_context->uri, drush_get_context('site'));
  $config->site_path = $tmp_dir . '/extracted/docroot/sites/' . $site_context->uri;
  $config->write();
  
  // Delete the user 1 credentials file.
  foreach (drush_scan_directory($tmp_dir . '/extracted/docroot', '#^credentials-.*\.txt$#', array('.', '..', 'CVS'), 0, FALSE) as $credentials_file => $v);
  provision_file()
    ->unlink($credentials_file)
    ->succeed('Removed the site credentials file')
    ->fail('Could not remove the site credentials file: @path');
    
  // Create the platform path to move our temp platform to.
  provision_file()
    ->mkdir(d()->root)
    ->succeed('Created the platform path.')
    ->fail('Failed to create the platform path.');
  
  // So now we have the file all set up and working, we can move it over to the platform location.
  provision_file()
    ->switch_paths($tmp_dir . '/extracted/docroot', d()->root)
    ->succeed('Moved the temporary extracted platform to the new platform path.')
    ->fail('Could not move the temporary extracted platform to the new platform path.');
    
  // Remove our temporary directory.
  drush_shell_exec('rm -r %s', $tmp_dir);
    
  // Delete the provision context for the site that was created for us.
  drush_backend_invoke_args('provision-save', array($site_context->name), array('delete' => TRUE, 'root' => null, 'uri' => null));

}

/**
 * Cross-platform compatible helper function to recursively create a directory tree.
 * @see http://theserverpages.com/php/manual/en/function.mkdir.php#50383
 */
function drupal_gardens_mkdir($path) {
  return is_dir($path) || (drupal_gardens_mkdir(dirname($path)) && drush_shell_exec('mkdir %s', $path));
}